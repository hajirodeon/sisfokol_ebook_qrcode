<?php
session_start();

//////////////////////////////////////////////////////////////////////
// SISFOKOL-EBOOK-QRCODE v1.0                                       //
// SISFOKOL khusus sistem perpustakaan ebook pdf sederhana,         //
// dengan metode login peminjam dengan qrcode scan.                 //
//////////////////////////////////////////////////////////////////////
// Dikembangkan oleh : Agus Muhajir                                 //
// E-Mail : hajirodeon@gmail.com                                    //
// HP/SMS/WA : 081-829-88-54                                        //
// source code :                                                    //
//   http://github.com/hajirodeon                                   //
//   http://gitlab.com/hajirodeon                                   //
//////////////////////////////////////////////////////////////////////



//ambil nilai
require("inc/config.php");
require("inc/fungsi.php");
require("inc/koneksi.php");


//hapus session
session_unset();
session_destroy();


//re-direct
xloc($sumber);
exit();
?>