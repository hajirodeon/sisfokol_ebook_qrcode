-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 29 Agu 2021 pada 01.58
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisfokol_ebook_qrcode`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `adminx`
--

CREATE TABLE `adminx` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `usernamex` varchar(15) NOT NULL DEFAULT '',
  `passwordx` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `adminx`
--

INSERT INTO `adminx` (`kd`, `usernamex`, `passwordx`) VALUES
('21232f297a57a5a743894a0e4a801fc3', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_pinjam`
--

CREATE TABLE `item_pinjam` (
  `kd` varchar(50) NOT NULL,
  `orang_kd` varchar(50) DEFAULT NULL,
  `orang_qrcode` varchar(100) DEFAULT NULL,
  `orang_kode` varchar(50) DEFAULT NULL,
  `orang_nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `item_kd` varchar(50) DEFAULT NULL,
  `item_kode` varchar(100) DEFAULT NULL,
  `item_qrcode` varchar(100) DEFAULT NULL,
  `item_nama` varchar(100) DEFAULT NULL,
  `ket` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_item`
--

CREATE TABLE `m_item` (
  `kd` varchar(50) NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `ringkasan` longtext DEFAULT NULL,
  `filex1` longtext DEFAULT NULL,
  `filex_pdf` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `qrcode` varchar(100) DEFAULT NULL,
  `jml_dilihat` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `m_item`
--

INSERT INTO `m_item` (`kd`, `kode`, `nama`, `ringkasan`, `filex1`, `filex_pdf`, `postdate`, `qrcode`, `jml_dilihat`) VALUES
('7a93495c625690b86dcd61a3141094bb', 'buk008', 'judul lagi', 'ringkasannya...', NULL, NULL, '2020-12-10 23:37:35', '8434buk008', NULL),
('f9c8178195493e7c9c3db3d129a0415f', 'buk003', 'judul tiga', 'ringkasannya...', 'f9c8178195493e7c9c3db3d129a0415f-1.jpg', NULL, '2020-12-10 23:34:33', '7234buk003', '1'),
('c9e9c7adbfa929864554c0bf4bc7dcff', 'buk002', 'judul dua', 'ringkasannya...', 'c9e9c7adbfa929864554c0bf4bc7dcff-1.jpg', NULL, '2020-12-10 23:57:37', '9199buk002', NULL),
('3638840b643747b37047b78a2fdffbb4', 'buk001', 'judul satu', 'ringkasannya...', '3638840b643747b37047b78a2fdffbb4-1.jpg', NULL, '2020-12-10 23:33:46', '2250buk001', '1'),
('187ad284270702c447193ec31d45934a', 'buk007', 'judul buku', 'ringkasannya...', '187ad284270702c447193ec31d45934a-1.jpg', NULL, '2020-12-10 23:56:46', '3250buk007', '1'),
('7e47eafb5ed238d6ee3304ff1752004d', 'buk006', 'judul enam', 'ringkasannya...', NULL, NULL, '2020-12-10 23:37:35', '7828buk006', NULL),
('c211242c14bd305783b5fc3decbd4d3d', 'dddd', 'dddd', 'dddd', 'c211242c14bd305783b5fc3decbd4d3d-1.jpg', NULL, '2020-12-10 23:56:25', '9634dddd', '9');

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_orang`
--

CREATE TABLE `m_orang` (
  `kd` varchar(50) NOT NULL,
  `qrcode` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `filex1` longtext DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `jabatan` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `m_orang`
--

INSERT INTO `m_orang` (`kd`, `qrcode`, `kode`, `nama`, `filex1`, `postdate`, `jabatan`) VALUES
('8bf4a5ef247b078be2efb16aa30a2ac5', '56111', '1', 'coba satu', '8bf4a5ef247b078be2efb16aa30a2ac5-1.jpg', '2020-12-10 22:50:32', 'GURU'),
('4eb0a8eced9331b35295b5144320a87c', '49452', '2', 'coba dua', '4eb0a8eced9331b35295b5144320a87c-1.jpg', '2020-12-10 22:50:52', 'GURU'),
('c5729fb0b5ff6270a4d7e516dbaa93d0', '35033', '3', 'coba 3', 'c5729fb0b5ff6270a4d7e516dbaa93d0-1.jpg', '2020-12-10 22:51:08', 'SISWA'),
('c5e0fe6785c1f3d1273381689cc42507', '9829234', '234', 'bambang', NULL, '2020-12-10 23:27:46', 'SISWA'),
('ec5d304c957de8af3d17b69710ca9d25', '2740235', '235', 'sodikin', NULL, '2020-12-10 23:27:46', 'GURU'),
('5aa0f6e94f733313b24b5039a286e4e9', '2221236', '236', 'ahmad', NULL, '2020-12-10 23:27:46', 'GURU');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orang_login`
--

CREATE TABLE `orang_login` (
  `kd` varchar(50) NOT NULL,
  `orang_kd` varchar(50) DEFAULT NULL,
  `orang_kode` varchar(50) DEFAULT NULL,
  `orang_nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data untuk tabel `orang_login`
--

INSERT INTO `orang_login` (`kd`, `orang_kd`, `orang_kode`, `orang_nama`, `postdate`) VALUES
('48778d4764750af11d3d13d602d667ec', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 01:40:47'),
('f4f333abe4eff24d320d200dfc7c9cbb', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 01:40:53'),
('90ea4844b7de5e20aa86bf4dbbcd1f75', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 01:41:11'),
('24d9ef435559f8f434d56448bc15ed70', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 01:44:08'),
('8ea37b6d3129b9616bd7002545d5f628', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 01:44:28'),
('54cb4bf38d97892b3d5e141cab3eae8b', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 02:15:00'),
('361a7d7afc624f8b863152d18ef2edbd', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 02:41:08'),
('974ccafab194244aadeac809dacabe2c', '5aa0f6e94f733313b24b5039a286e4e9', '236', 'ahmad', '2020-12-11 02:41:32'),
('fa42fd513e7cf69edd534e576e1c6b7f', '4eb0a8eced9331b35295b5144320a87c', '2', 'coba dua', '2020-12-11 02:42:44'),
('f78c6bf9a8d2b3b328c10cdddbb8c272', '5aa0f6e94f733313b24b5039a286e4e9', '236', 'ahmad', '2020-12-11 02:48:21');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `adminx`
--
ALTER TABLE `adminx`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `item_pinjam`
--
ALTER TABLE `item_pinjam`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_item`
--
ALTER TABLE `m_item`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `m_orang`
--
ALTER TABLE `m_orang`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `orang_login`
--
ALTER TABLE `orang_login`
  ADD PRIMARY KEY (`kd`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
